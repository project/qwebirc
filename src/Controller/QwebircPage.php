<?php

namespace Drupal\qwebirc\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Jira_RestController.
 */
class QwebircPage extends ControllerBase {

  use StringTranslationTrait;

  /**
   * A configuration object containing samlauth settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;


  /**
   * QwebircPage constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user) {
    $this->config = $config_factory->get('qwebirc.settings');
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  public function getTitle() {
    return $this->config->get('qwebirc_page_title');
  }
  /**
   * qwebirc page.
   */
  public function getContent() {
    $account = $this->currentUser->getAccount();

    $url_args = [];
    $title = $this->config->get('qwebirc_page_title');
    $server = $this->config->get('qwebirc_server');
    $width = 'width="' . $this->config->get('qwebirc_width') . '"';
    $height = 'height="' . $this->config->get('qwebirc_height') . '"';

    if ($this->config->get('qwebirc_channel')) {
      $channels = preg_replace('/##/', '%23', $this->config->get('qwebirc_channel'));
      $channels = preg_replace('/#/', '', $channels);
      if ($channels != '') {
        $url_args['channels'] = $channels;
      }
    }
    if ($this->config->get('qwebirc_nicknamemode') === 1) {
      $url_args['randomnick'] = 1;
      if ($this->config->get('qwebirc_dialog') === TRUE) {
        $url_args['prompt'] = 1;
      }
    }
    elseif ($this->config->get('qwebirc_nicknamemode') === 2) {
      $url_args['nick'] = $this->config->get('qwebirc_nickname');
      if ($this->config->get('qwebirc_dialog') === TRUE || $this->config->get('qwebirc_nickname') === '') {
        $url_args['prompt'] = 1;
      }
    }
    elseif ($this->config->get('qwebirc_nicknamemode') === 3) {
      if (!isset($account) || $account->isAnonymous()) {
        $url_args['prompt'] = 1;
      }
      else {
        $url_args['nick'] = preg_replace('/ /', '_', $account->getAccountName());
        if ($this->config->get('qwebirc_dialog') === 1 || $account->getAccountName() === '') {
          $url_args['prompt'] = 1;
        }
      }
    }
    if (isset($url_args['nick']) && $url_args['nick'] == $account->getAccountName() && $account->id() === 1 && $this->config->get('qwebirc_nickname_admmask') != '') {
      //reset admin name to something else
      $url_args['nick'] = $this->config->get('qwebirc_nickname_admmask');
    }

    $url = '';
    foreach ($url_args as $param => $val) {
      if ($url != '') {
        $url .= '&';
      }
      else {
        $url = '?';
      }
      $url .= $param . '=' . $val;
    }
    $url = $server . $url;

    // TODO: Not sure if this is the best way to render. We allow HTML markup,
    // However, you must be an admin to set these config settings. So its ok?
    return [
      '#type' => 'inline_template',
      '#template' => '<div class="irc_welcome">{{welcome_text}}</div>'.
        '<div class="iframe"><iframe src="{{url}}" width="{{width}}" height="{{height}}">You must have iframes Enabled.</iframe></div>' .
        '<p>{{help_text}}</p>',
      '#context' => [
        'welcome_text' => new FormattableMarkup($this->config->get('qwebirc_welcome'), []),
        'url' => $url,
        'width' => $this->config->get('qwebirc_width'),
        'height' => $this->config->get('qwebirc_height'),
        'help_text' => new FormattableMarkup($this->config->get('qwebirc_help'), []),
      ],
    ];
  }
}
