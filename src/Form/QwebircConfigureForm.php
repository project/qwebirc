<?php

namespace Drupal\qwebirc\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure site information settings for this site.
 */
class QwebircConfigureForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['qwebirc.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'qwebirc_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $qwebirc_config = $this->config('qwebirc.settings');

    $form['qwebirc_page_title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('IRC Page Title'),
      '#default_value' => $qwebirc_config->get('qwebirc_page_title'),
      '#description' => $this->t('Header Title for the qwebirc Chat Page'),
      '#size' => 20,
      '#required' => TRUE,
    );
    $form['qwebirc_welcome'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('IRC welcome'),
      '#default_value' => $qwebirc_config->get('qwebirc_welcome'),
      '#description' => $this->t('Optional welcome information that appears above the chat window.'),
      '#required' => FALSE,
    );
    $form['qwebirc_channel'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Channel name'),
      '#default_value' => $qwebirc_config->get('qwebirc_channel'),
      '#description' => $this->t('Name of the IRC channel to access. use pound # signs for double pound channels ## only.'),
      '#size' => 20,
      '#required' => TRUE,
    );
    $form['qwebirc_width'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Chat window width'),
      '#default_value' => $qwebirc_config->get('qwebirc_width'),
      '#description' => $this->t('Width of the IRC chat window.'),
      '#size' => 5,
      '#required' => TRUE,
    );
    $form['qwebirc_height'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Chat window height'),
      '#default_value' => $qwebirc_config->get('qwebirc_height'),
      '#description' => $this->t('Height of the IRC chat window.'),
      '#size' => 5,
      '#required' => TRUE,
    );
    $form['qwebirc_help'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('IRC help'),
      '#default_value' => $qwebirc_config->get('qwebirc_help'),
      '#description' => $this->t('Optional help information that appears below the chat window.'),
      '#required' => FALSE,
    );

    $form['qwebirc_nicknamemode'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Nickname Mode'),
      '#description' => $this->t('At startup would you like the client to use a random nickname, a preset nickname or a nickname of the users choice?'),
      '#default_value' => $qwebirc_config->get('qwebirc_nicknamemode'),
      '#options' => [
        0 => $this->t('Make the user choose a nickname.'),
        1 => $this->t('Use a random nickname, e.g. qwebirc12883.'),
        2 => $this->t('Use a preset nickname of your choice.'),
        3 => $this->t('Use nickname of currently logged in user.')
      ],
    );

    $form['qwebirc_nickname'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Preset Nickname'),
      '#description' => $this->t('Enters a preset nickname of your choice. ONLY WORKS WITH preset nickname, option three of nickname mode'),
      '#default_value' => $qwebirc_config->get('qwebirc_nickname', ''),
    );
    $form['qwebirc_nickname_admmask'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Admin Nickname Mask'),
      '#description' => $this->t('Enters a preset nickname for user 1... if you desire to use irc as that user'),
      '#default_value' => $qwebirc_config->get('qwebirc_nickname_admmask', ''),
    );
    $form['qwebirc_server'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('qwebirc server'),
      '#default_value' => $qwebirc_config->get('qwebirc_server'),
      '#description' => $this->t('the servername for the qwebirc client. Freenode is our default. YOU MUST use the full URI (http or https://) Please also include the trailing slash "/"'),
      '#size' => 64,
      '#required' => TRUE,
    );

    $form['qwebirc_dialog'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Display connect dialog?'),
      '#description' => $this->t('Do you want the user to be shown the connect dialog (with the values you have supplied pre-entered) or just a connect confirmation?'),
      '#default_value' => $qwebirc_config->get('qwebirc_dialog'),
      '#options' => [
        FALSE => $this->t('Connect without displaying the dialog.'),
        TRUE => $this->t('Show the connect dialog.'),
      ],
    );
    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('qwebirc_width'))) {
      $form_state->setErrorByName('qwebirc_width', $this->t('Please enter a number.'));
    }
    if (!is_numeric($form_state->getValue('qwebirc_height'))) {
      $form_state->setErrorByName('qwebirc_height', $this->t('Please enter a number.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $qwebirc_config = $this->config('qwebirc.settings');
    $qwebirc_config
      ->set('qwebirc_page_title', $form_state->getValue('qwebirc_page_title'))
      ->set('qwebirc_welcome', $form_state->getValue('qwebirc_welcome'))
      ->set('qwebirc_channel', $form_state->getValue('qwebirc_channel'))
      ->set('qwebirc_width', $form_state->getValue('qwebirc_width'))
      ->set('qwebirc_height', $form_state->getValue('qwebirc_height'))
      ->set('qwebirc_help', $form_state->getValue('qwebirc_help'))
      ->set('qwebirc_nicknamemode', $form_state->getValue('qwebirc_nicknamemode'))
      ->set('qwebirc_nickname', $form_state->getValue('qwebirc_nickname'))
      ->set('qwebirc_nickname_admmask', $form_state->getValue('qwebirc_nickname_admmask'))
      ->set('qwebirc_server', $form_state->getValue('qwebirc_server'))
      ->set('qwebirc_dialog', $form_state->getValue('qwebirc_dialog'));
    $qwebirc_config->save();

    parent::submitForm($form, $form_state);
  }
}
